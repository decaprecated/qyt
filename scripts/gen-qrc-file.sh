#!/bin/bash

qrc="<!-- generated with $0 $* --><!DOCTYPE RCC><RCC version=\"1.0\"><qresource>"

shopt -s globstar

for d in $@; do
    [[ -d $d ]] || exit 1

    for f in "$d"/**/*; do
        qrc+="<file>$f</file>"
    done
done

qrc+="</qresource></RCC>"

echo $qrc
