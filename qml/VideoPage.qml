import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: column

    property string title
    property string summary
    property string video_id
    property url thumbnail

    signal clicked

    Label {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
        text: title
        font.pixelSize: 32
        wrapMode: Text.Wrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    Button {
        id: button

        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
        Layout.fillHeight: true
        Layout.fillWidth: true

        contentItem: Image {
            id: image

            anchors.fill: parent
            anchors.margins: 8
            source: thumbnail
            fillMode: Image.PreserveAspectFit

            Image {
                anchors.fill: parent
                source: "qrc:/images/play-icon.png"
                fillMode: Image.PreserveAspectFit
            }

        }

        onClicked: {
            button.enabled = false
            video_model.show_video(video_id, code => button.enabled = true)
        }
    }
    RowLayout {
        Layout.fillWidth: true

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
        Button {
            Layout.fillHeight: true
            text: "Open in YouTube"

            onClicked: Qt.openUrlExternally(`https://www.youtube.com/watch?v=${video_id}`)
        }
        Button {
            Layout.fillHeight: true
            text: "Open in Invidious"

            onClicked: Qt.openUrlExternally(`${config.get_invidious_instance()}/watch?v=${video_id}`)
        }
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
    ScrollView {
        id: scroll

        Layout.fillWidth: true 
        Layout.maximumHeight: column.height / 3 
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        contentWidth: scroll.availableWidth
        clip: true

        Label {
            anchors.fill: parent
            text: linkify(summary)
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter

            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}
