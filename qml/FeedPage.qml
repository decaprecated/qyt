import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ListView {
    id: feed_page

    visible: true
    spacing: 16
    anchors.leftMargin: 16
    orientation: ListView.Horizontal
    snapMode: ListView.SnapToItem
    clip: true
    model: channel_model
    delegate: ColumnLayout {
        id: channel

        width: Math.max(250, (feed_page.width - (channel_model.theoretical_length() * feed_page.spacing)) / channel_model.theoretical_length())
        height: feed_page.height

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: name
            font.pixelSize: 36
        }
        ListView {
            id: videos

            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: 8
            orientation: ListView.Vertical
            snapMode: ListView.SnapToItem
            model: video_model
            delegate: Button {
                id: button

                width: videos.width
                height: videos.width * 9 / 16 + 24
                contentItem: ColumnLayout {
                    spacing: 0
                    anchors.fill: parent
                    anchors.margins: 8

                    Image {
                        id: button_image

                        source: thumbnail
                        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                        Layout.maximumHeight: button.height
                        Layout.maximumWidth: button.width - 24
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        fillMode: Image.PreserveAspectFit
                    }
                    Label {
                        id: button_text

                        Layout.maximumWidth: button.width - 24
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                        text: title
                        wrapMode: Text.Wrap
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                }

                
                onClicked: {
                    var component = Qt.createComponent("qrc:/qml/VideoPage.qml");
                    if (component.status == Component.Ready) {
                        var video_view = component.createObject(stack);
                        video_view.title = title
                        video_view.summary = summary
                        video_view.thumbnail = thumbnail
                        video_view.video_id = video_id
                        stack.push(video_view)
                    } else {
                        error(`Error generating video page for ${video_id}: ${component.errorString()}`)
                    }
                }

            }

        }
    }

    Component.onCompleted: {
        if (channel_model.rowCount() == 1) {
            feed_page.spacing = 0
            feed_page.anchors.marginLeft = 0
        }
    }
}
