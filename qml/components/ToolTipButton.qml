import QtQuick 2.15
import QtQuick.Controls 2.12

Button {
    implicitWidth: height
    icon.name: "help-about"

    onClicked: ToolTip.visible = !ToolTip.visible
}
