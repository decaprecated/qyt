import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "./components"

ColumnLayout {
    Label {
        id: message
        text: ""

        Layout.fillWidth: true
    }

    RowLayout {
        Layout.fillWidth: true

        ToolTipButton {
            ToolTip.text: "youtube.com/channel/<b>xxxxx</b>/"
        }
        Label { text: "Channel ID:" }
        TextField {
            id: channel_id_input

            Layout.fillWidth: true
        }
    }
    Button {
        id: button

        text: "Add"
        icon.name: "list-add"

        onClicked: {
            button.enabled = false
            message.text = ""

            var channel_id = channel_id_input.text

            if (!channel_id) {
                message.text = "Please input a channel ID."
                button.enabled = true
                return
            }

            if (channel_model.contains(channel_id)) {
                message.text = "You are already subscribed to this channel."
                button.enabled = true
                return
            }

            validate_channel_id(channel_id, valid => {
                if (valid) {
                    message.text = "Added channel successfully."
                    channel_model.add_channel(channel_id)
                } else {
                    message.text = "Invalid channel ID."
                }
                button.enabled = true
            })
        }
    }
    // filler
    Item { Layout.fillHeight: true }

    Component.onCompleted: {
    }
}
