import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

import "qrc:/qml/"

ApplicationWindow {
    id: window

    width: 800
    height: 600
    visible: true
    title: "qyt"

    header: ToolBar {
        RowLayout {
            ToolButton {
                icon.name: "go-home"

                onClicked: {
                    stack.anchors.margins.rightMargin = 0
                    stack.pop(feed_page)
                }
            }
            ToolButton {
                icon.name: "settings-configure"

                onClicked: {
                    if (stack.currentItem != settings_page) stack.push(settings_page)
                }
            }
            ToolButton {
                icon.name: "list-add"

                onClicked: {
                    if (stack.currentItem != add_channel_page) stack.push(add_channel_page)
                }
            }
            ToolButton {
                icon.name: "view-refresh"

                onClicked: channel_model.refresh()
            }
        }
    }

    StackView {
        id: stack

        anchors.fill: parent
        anchors.margins: 16
        initialItem: feed_page

        FeedPage {
            id: feed_page
        }
        SettingsPage {
            id: settings_page

            StackView.visible: this == stack.currentItem
        }
        AddChannelPage {
            id: add_channel_page

            StackView.visible: this == stack.currentItem
        }
    }

    SystemTrayIcon {
        id: tray

        visible: true
        icon.name: "qyt-tray-icon"
        icon.source: "qrc:/images/tray-icon-thin.png"
    }
    Component.onCompleted: {
        notifications.register(tray.showMessage)
    }
}
