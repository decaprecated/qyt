import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "./components"

ColumnLayout {
    Label {
        id: message

        Layout.fillWidth: true
    }
    RowLayout {
        Layout.fillWidth: true

        ToolTipButton { ToolTip.text: "See invidious.io for more info." }
        CheckBox {
            id: invidious_checked

            Layout.fillWidth: true
            text: "Use Invidious"
            checked: config.get_invidious()
        }
    }
    RowLayout {
        Layout.fillWidth: true

        Label { text: "Invidious Instance:" }
        TextField {
            id: invidious_instance_input

            Layout.fillWidth: true
            enabled: invidious_checked.checked
            text: config.get_invidious_instance()
        }
    }
    RowLayout {
        Layout.fillWidth: true

        ToolTipButton { ToolTip.text: "How often to check the RSS feed." }
        Label { text: "Polling Rate (minutes):" }
        TextField {
            id: polling_rate_input

            Layout.fillWidth: true
            text: config.get_polling_rate()
            validator: IntValidator { bottom: 1 }
        }
    }
    CheckBox {
        id: notifications_checked

        Layout.fillWidth: true
        text: "Enable Notifications"
        checked: config.get_notifications()
    }
    RowLayout {
        Layout.fillWidth: true

        ToolTipButton { ToolTip.text: "Available parameters: %video_id, %link, %local_path\nUnless %local_path is used, the video won't be downloaded by qyt." }
        Label { text: "Video Player Command" }
        TextField {
            id: video_command_input

            Layout.fillWidth: true
            text: config.get_video_command()
        }
    }
    Button {
        id: button

        text: "Save"

        onClicked: {
            button.enabled = false
            message.text = ""
            

            if (!polling_rate_input.text) {
                message.text = "Please input the polling rate."
                button.enabled = true
                return
            }

            if (!video_command_input.text) {
                message.text = "Please input the video command."
                button.enabled = true
                return
            }
            
            function save() {
                config.set_invidious(invidious_checked.checked)
                config.set_notifications(notifications_checked.checked)
                config.set_polling_rate(parseInt(polling_rate_input.text))
                config.set_video_command(video_command_input.text)

                config.save()
                button.enabled = true
                message.text = "Save successful."
            }

            if (invidious_checked.checked) {
                var invidious_instance = invidious_instance_input.text

                if (!invidious_instance) {
                    message.text = "Please input the invidious instance."
                    button.enabled = true
                    return
                }

                if (invidious_instance.endsWith('/')) invidious_instance = invidious_instance.slice(0, -1)

                validate_invidious_instance(invidious_instance, valid => {
                    if (valid) {
                        config.set_invidious_instance(invidious_instance)
                        save()
                    } else {
                        message.text = "Check your invidious instance."
                        button.enabled = true
                    }
                })
            } else {
                save()
            }

        }
    }
    // filler
    Item { Layout.fillHeight: true }

    Component.onCompleted: {
    }
}
