#!/bin/sh

yt-dlp "$1" -o -  --extractor-args youtube:player_client=android --throttled-rate 100K | mpv --profile=youtube -
# vim: ft=sh
