from __future__ import annotations

import re
import logging
import typing

from pathlib import Path

from PySide2.QtCore import QObject, Slot
from PySide2.QtQml import QJSValue

from .Threads import FeedThread

if typing.TYPE_CHECKING:
    from .Configuration import Configuration

TMP_DIR: Path = Path("/tmp") / "qyt"
URL_REGEX = (
    r"(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|"
    r"(?:%[0-9a-fA-F][0-9a-fA-F]))+)"
)

if not TMP_DIR.is_dir():
    TMP_DIR.mkdir()


class Bridge(QObject):
    def __init__(self, config: Configuration, parent=None):
        super().__init__(parent)
        self.config = config

    @Slot(str, result=str)
    def linkify(_, text: str) -> str:
        """
        converts plaintext links to <a> tags and replaces newlines with <br>
        tags
        """
        text = re.sub(URL_REGEX, r'<a href="\1">\1</a>', text)
        text = text.replace("\n", "<br />")
        return text

    @Slot(result=str)
    def tmp_dir(_):
        return str(TMP_DIR.absolute())

    @Slot(str, "QJSValue")
    def validate_invidious_instance(self, invidious_instance, callback):
        def cb(feed):
            valid = "status" in feed and int(feed["status"]) == 200
            logging.debug(f"{invidious_instance} validity: {valid}.")
            ThreadSafeCB.call(callback, valid)

        # TODO maybe don't hardcode channel/find another way to test
        # rss, pretty low priority
        feed_thread = FeedThread(
            invidious_instance + "/feed/channel/UC3tNpTOHsTnkmbwztCs30sA", self
        )
        feed_thread.fetched.connect(cb)
        feed_thread.start()

    @Slot(str, "QJSValue")
    def validate_channel_id(self, channel_id, callback):
        def cb(feed):
            valid = int(feed["status"]) == 200
            logging.debug(f"{channel_id} validity: {valid}")
            ThreadSafeCB.call(callback, valid)

        feed_thread = FeedThread(self.config.rss_endpoint() + channel_id, self)
        feed_thread.fetched.connect(cb)
        feed_thread.start()

    @Slot(str)
    def debug(_, msg):
        logging.debug(msg)

    @Slot(str)
    def info(_, msg):
        logging.info(msg)

    @Slot(str)
    def warning(_, msg):
        logging.warning(msg)

    @Slot(str)
    def error(_, msg):
        logging.error(msg)


class ThreadSafeCB(QObject):
    """
    Helper class to call a QML JavaScript callback on the correct thread by using slots
    """

    cb: QJSValue
    args: typing.List[QJSValue]

    def __init__(self, cb: QJSValue, args: typing.List[QJSValue]):

        self.args = args
        self.cb = cb

    @Slot()
    def execute(self):
        self.cb.call(self.args)

    @staticmethod
    def call(callback: QJSValue, *args: typing.Any):
        ThreadSafeCB(
            callback,
            [a if isinstance(a, QJSValue) else QJSValue(a) for a in args],
        ).execute()
