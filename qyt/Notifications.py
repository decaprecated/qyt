import typing as t

from PySide2.QtCore import QObject, Slot
from PySide2.QtQml import QJSValue


_method: t.Optional[QJSValue] = None


@Slot(str, str)
def show_message(title: str, message: str):
    global _method

    if not _method:
        return

    _method.call([QJSValue(title), QJSValue(message)])


class Bridge(QObject):
    @Slot("QJSValue")
    def register(_, method: QJSValue):
        global _method

        _method = method
