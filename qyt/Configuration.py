from __future__ import annotations

import typing as t
import logging

from PySide2.QtCore import QObject, Slot

if t.TYPE_CHECKING:
    from PySide2.QtCore import QSettings

INVIDIOUS_DEFAULT: str = "https://invidious.snopyta.org"


class Configuration(QObject):
    """
    Helper class to hold our QSettings values

    Members:

    - invidious: is invidious enabled
    - invidious_instance: what invidious instance to use
    - host: invidious_instance or youtube
    - rss_endpoint: endpoint to get RSS feed, needs channel_id appended
    - polling_rate: how often to check the RSS feed
    - notifications: if notifications are enabled
    - subscribed_channels: list of channel ids to watch
    - video_command: external video command template"""

    invidious: bool
    invidious_instance: str
    polling_rate: int
    notifications: bool
    subscribed_channels: t.List[str]
    video_command: str

    def __init__(self, settings: QSettings, parent: QObject = None):
        super().__init__(parent)
        self.settings = settings
        self.load()

    """
    helper method to cast a QSettings value to a string
    """

    def _value(self, path: str, default: str) -> str:
        return t.cast(str, self.settings.value(path, default))

    @Slot(result=str)  # type: ignore
    def host(self) -> str:
        return (
            self.invidious_instance
            if self.invidious
            else "https://www.youtube.com"
        )

    @Slot(result=str)  # type: ignore
    def rss_endpoint(self) -> str:
        return self.host() + (
            "/feed/channel/"
            if self.invidious
            else "/feeds/videos.xml?channel_id="
        )

    def load(self):
        logging.info(
            f"Loading configuration from file {self.settings.fileName()}."
        )
        self.invidious = bool(int(self._value("Invidious/enabled", "0")))

        self.invidious_instance = self._value(
            "Invidious/instance", INVIDIOUS_DEFAULT
        )
        self.polling_rate = int(
            self._value("General/polling", "30" if self.invidious else "5")
        )

        self.notifications = bool(
            int(self._value("General/notifications", "0"))
        )

        subscribed_channels = self.settings.value("Channels/subscribed", [])
        if isinstance(subscribed_channels, str):
            self.subscribed_channels = [subscribed_channels]
        else:
            self.subscribed_channels = t.cast(t.List[str], subscribed_channels)

        self.video_command = self._value("General/command", "mpv --ytdl %link")

    @Slot()
    def save(self):
        logging.info(f"Saving configuration to file {self.settings.fileName()}")
        self.settings.setValue("Invidious/enabled", int(self.invidious))
        self.settings.setValue("Invidious/instance", self.invidious_instance)
        self.settings.setValue("General/polling", self.polling_rate)
        self.settings.setValue("General/notifications", int(self.notifications))

        self.settings.setValue("Channels/subscribed", self.subscribed_channels)
        self.settings.setValue("General/command", self.video_command)

    @Slot(result=bool)
    def get_invidious(self) -> bool:
        return self.invidious

    @Slot(bool)
    def set_invidious(self, i: bool):
        self.invidious = i

    @Slot(result=str)
    def get_invidious_instance(self) -> str:
        return self.invidious_instance

    @Slot(str)
    def set_invidious_instance(self, s: str):
        self.invidious_instance = s

    @Slot(result=int)
    def get_polling_rate(self) -> int:
        return self.polling_rate

    @Slot(int)
    def set_polling_rate(self, p: int):
        self.polling_rate = p

    @Slot(result=bool)
    def get_notifications(self) -> bool:
        return self.notifications

    @Slot(bool)
    def set_notifications(self, n: bool):
        self.notifications = n

    @Slot(result=str)
    def get_video_command(self) -> str:
        return self.video_command

    @Slot(str)
    def set_video_command(self, s: str):
        self.video_command = s
