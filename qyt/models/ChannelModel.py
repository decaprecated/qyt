from __future__ import annotations

import typing as t
import logging

from PySide2.QtCore import Qt, Slot, QTimer

from .OrderedListModel import OrderedListModel
from .VideoModel import VideoModel
from .. import Notifications
from ..Threads import FeedThread

if t.TYPE_CHECKING:
    from ..Configuration import Configuration


class ChannelModel(OrderedListModel):

    ChannelIdRole = Qt.UserRole + 1
    NameRole = Qt.UserRole + 2
    VideoModelRole = Qt.UserRole + 3

    def __init__(self, config: Configuration, parent=None):
        super().__init__(config, parent)

        for channel_id in config.subscribed_channels:
            self.add_channel(channel_id)

        self.refresh_timer = QTimer(self)
        self.refresh_timer.timeout.connect(self.refresh)
        self.refresh_timer.setInterval(self.config.polling_rate * 60000)

    @Slot(str)
    def add_channel(self, channel_id: str):
        # TODO migrate to config properties
        if channel_id not in self.config.subscribed_channels:
            self.config.subscribed_channels.append(channel_id)
            self.config.save()

        self.wanted_order.append(channel_id)

        def cb(feed):
            logging.info(f"Parsed feed for {channel_id}.")

            self.update_item(
                channel_id,
                {
                    self.VideoModelRole: VideoModel(feed, self.config, self),
                    self.NameRole: feed["feed"]["author"],
                },
            )

        feed_thread = FeedThread(self.config.rss_endpoint() + channel_id, self)
        feed_thread.fetched.connect(cb)
        feed_thread.start()

    def roleNames(self):
        return {
            self.ChannelIdRole: b"channel_id",
            self.NameRole: b"name",
            self.VideoModelRole: b"video_model",
        }

    @Slot()
    def refresh(self):
        r = self.config.polling_rate * 60000
        if self.refresh_timer.interval() != r:
            logging.debug(
                "Updated polling rate in timer: "
                f"{{old: {self.refresh_timer.interval()}, "
                f"new: {r}"
            )
            self.refresh_timer.setInterval(r)

        for channel_id in self.current_order:
            self.refresh_channel(channel_id)

    @Slot(str)
    def refresh_channel(self, channel_id):
        if channel_id not in self.current_order:
            return

        logging.info(f"Refreshing channel {channel_id}.")

        video_model = self._data[channel_id][self.VideoModelRole]
        top_video_id = video_model.current_order[0]

        logging.debug(f"Top video id for {channel_id}: {top_video_id}.")

        def cb(feed):
            i = 0

            for entry in feed["entries"]:
                if entry["yt_videoid"] == top_video_id:
                    break

                logging.info(
                    f"Fetched new video {entry['yt_videoid']} for "
                    f"{channel_id}."
                )
                video_model.insert_video(i, entry)
                Notifications.show_message("New Video", entry["title"])

            logging.info(
                f"Updated {channel_id} with {i} new video"
                + ("." if i == 1 else "s.")
            )

        feed_thread = FeedThread(self.config.rss_endpoint() + channel_id, self)
        feed_thread.fetched.connect(cb)
        feed_thread.start()
