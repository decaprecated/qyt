from __future__ import annotations

import typing as t
import logging
import re
from pathlib import Path

from PySide2.QtCore import Qt, Slot

from .OrderedListModel import OrderedListModel
from ..util import TMP_DIR, ThreadSafeCB
from ..Threads import YTDLThread, SubprocessThread


if t.TYPE_CHECKING:
    from ..Configuration import Configuration
    from PySide2.QtQml import QJSValue

class VideoModel(OrderedListModel):
    VideoIdRole = Qt.UserRole + 1
    TitleRole = Qt.UserRole + 2
    SummaryRole = Qt.UserRole + 3
    ThumbnailRole = Qt.UserRole + 4

    video_locks: t.List[str] = []

    def __init__(self, feed, config: Configuration, parent=None):
        super().__init__(config, parent)

        for i, entry in enumerate(feed["entries"]):
            self.insert_video(i, entry)

    def update_video(self, video_id, entry):
        title = entry["title"]
        summary = entry["summary"]

        if self.config.invidious:
            summary = re.sub(r"(\n|.)*<p.*>((\n|.)+)<\/p>", r"\2", summary)

        def finish(thumbnail: Path):
            self.update_item(
                video_id,
                {
                    self.TitleRole: title,
                    self.SummaryRole: summary,
                    self.ThumbnailRole: thumbnail.absolute().as_uri(),
                },
            )

        thumbnail_glob = list(TMP_DIR.glob(video_id + "*"))

        if len(thumbnail_glob):
            finish(thumbnail_glob[0])
            return

        def cb(info):
            logging.info(f"Downloaded thumbnail for {video_id}.")

            thumbnail = Path(info["thumbnails"][-1]["filename"])
            finish(thumbnail)

        thumbnail_dl_thread = YTDLThread(
            {
                "writethumbnail": True,
                "skip_download": True,
                "outtmpl": str(TMP_DIR / "%(id)s"),
            },
            f"{self.config.host()}/watch?v={video_id}",
            self,
        )
        thumbnail_dl_thread.downloaded.connect(cb)
        thumbnail_dl_thread.start()

    def insert_video(self, i, entry):
        video_id = entry["yt_videoid"]
        self.wanted_order.insert(i, video_id)
        self.update_video(video_id, entry)

    def roleNames(self):
        return {
            self.VideoIdRole: b"video_id",
            self.TitleRole: b"title",
            self.SummaryRole: b"summary",
            self.ThumbnailRole: b"thumbnail",
        }

    @Slot(str, "QJSValue")
    def show_video(self, video_id: str, callback: t.Optional[QJSValue] = None):
        if video_id in self.video_locks:
            return

        self.video_locks.append(video_id)

        link: str = f"{self.config.host()}/watch?v={video_id}"

        command: str = self.config.video_command
        command = command.replace("%video_id", video_id).replace("%link", link)

        def cb(code):
            self.video_locks.remove(video_id)
            logging.info(f"Video player exited with code {code}")
            if callback:
                ThreadSafeCB.call(callback, code)

        def start(command: str):
            logging.info(f"Calling `{command}`")
            video_thread = SubprocessThread(command.split(" "), self)
            video_thread.exited.connect(cb)
            video_thread.start()

        if "%local_path" in command:
            glob = list(TMP_DIR.glob(f"video-{video_id}*"))

            if len(glob):
                start(command.replace("%local_path", str(glob[0])))
                return

            def dl_cb(info):
                if "ext" in info and info["ext"]:
                    local_path = str(
                        TMP_DIR / f"video-{video_id}.{info['ext']}"
                    )
                    logging.debug(f"Downloaded to {local_path}")
                    start(command.replace("%local_path", local_path))
                else:
                    cb(-1)

            video_dl_thread = YTDLThread(
                {"outtmpl": str(TMP_DIR / "video-%(id)s")}, link, self
            )
            video_dl_thread.downloaded.connect(dl_cb)
            video_dl_thread.start()
        else:
            start(command)


