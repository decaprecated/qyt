from __future__ import annotations

import typing as t

from PySide2.QtCore import QObject, QAbstractListModel, Qt, Slot, QModelIndex

if t.TYPE_CHECKING:
    from ..Configuration import Configuration


class OrderedListModel(QAbstractListModel):
    KeyRole = Qt.UserRole + 1

    def __init__(self, config: Configuration, parent: QObject = None):
        super().__init__(parent)
        self.config = config

        self.wanted_order = []
        self.current_order = []
        self._data = {}
        self.offset = 0

    def rowCount(self, _=None):
        return len(self._data)

    def data(self, index, role=Qt.DisplayRole):
        i = index.row()
        if i < 0 or i >= self.rowCount():
            return

        key = self.current_order[i]
        if role == self.KeyRole:
            return key

        data = self._data[key]

        if role in data:
            return data[role]

    def update_item(self, key, item):
        if key not in self.wanted_order or key in self.current_order:
            return

        i = self.wanted_order.index(key)
        upper = i

        if i >= len(self.current_order):
            upper = len(self.current_order) - 1

        j = 0

        while j <= upper and self.wanted_order.index(self.current_order[j]) < i:
            j += 1

        self.beginInsertRows(QModelIndex(), j, j)
        self._data[key] = item
        self.current_order.insert(j, key)
        self.endInsertRows()

    @Slot(result=int)
    def theoretical_length(self):
        return len(self.wanted_order)

    @Slot(str, result=bool)
    def contains(self, key):
        return key in self._data
