#!/usr/bin/env python3
import logging

from PySide2.QtCore import QSettings, QTimer, QUrl
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

from . import Notifications, qrc
from .Configuration import Configuration
from .models.ChannelModel import ChannelModel
from .util import Bridge


assert qrc


def main():
    logging.basicConfig(
        format="%(levelname)s - [qyt]: %(message)s", level=logging.DEBUG
    )

    app = QGuiApplication([])
    app.setApplicationName("qyt")
    app.setOrganizationName("qyt")

    config = Configuration(QSettings(), app)
    channel_model = ChannelModel(config, app)

    engine = QQmlApplicationEngine()

    engine.rootContext().setContextProperty("channel_model", channel_model)
    engine.rootContext().setContextProperty("config", config)
    engine.rootContext().setContextProperty(
        "notifications", Notifications.Bridge(app)
    )
    engine.rootContext().setContextObject(Bridge(config, app))

    polling_timer = QTimer(app)
    polling_timer.timeout.connect(channel_model.refresh)
    polling_timer.start(config.polling_rate * 60 * 1000)

    logging.debug("Loading QML...")
    engine.load(QUrl.fromLocalFile(":/qml/MainView.qml"))
    if not len(engine.rootObjects()):
        logging.error("Error loading QML")
        return
    logging.info("QML Loaded.")
    app.exec_()


if __name__ == "__main__":
    main()
