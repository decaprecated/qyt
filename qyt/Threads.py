from typing import List
import feedparser as fp
import youtube_dl as yt
import subprocess as sp

from PySide2.QtCore import QThread, Signal, QObject


class FeedThread(QThread):
    """
    QThread to fetch an RSS feed

    emits fetched(feed) when done
    """

    fetched = Signal(object)

    def __init__(self, rss_url: str, parent: QObject):
        super().__init__(parent)

        self.setObjectName("qytFeedThread")
        self.rss_url = rss_url

    def run(self):
        try:
            feed = fp.parse(self.rss_url)
        except Exception as e:
            print(e)
            self.fetched.emit({"status": 400})
            return
        self.fetched.emit(feed)


class YTDLThread(QThread):
    """
    QThread to download youtube videos with youtube_dl with specified options

    emits downloaded(info) when done
    """

    downloaded = Signal(object)

    def __init__(self, options, link: str, parent: QObject):
        super().__init__(parent)

        self.setObjectName("qytYTDLThread")
        self.yt_dl = yt.YoutubeDL(options)
        self.link = link

    def run(self):
        with self.yt_dl:
            info = self.yt_dl.extract_info(self.link)

        self.downloaded.emit(info)


class SubprocessThread(QThread):
    """
    QThread to call a subprocess

    emits exited(code) when done.
    """

    exited = Signal(int)

    def __init__(self, command: List[str], parent: QObject):
        super().__init__(parent)

        self.command = command

    def run(self):
        output = sp.run(self.command)
        self.exited.emit(output.returncode)
