# qyt

Qt application to subscribe to YouTube channels.

## Features

- supports invidious and youtube
- supports your external video player

## Note

Use at your own risk, this program works for me but some of the decisions made are not great.

## Usage

1. `git clone` this repository
2. create a virtualenv via `python -m venv venv`
    - activate it with `source venv/bin/activate`
4. install the dependencies via `pip install -r requirements.txt`
5. run via `python -m qyt`

## Dependencies

- `PySide2`
- `feedparser`
- `youtube-dl`


[discuss on Matrix](https://matrix.to/#/#qyt:matrix.org?via=matrix.org)
